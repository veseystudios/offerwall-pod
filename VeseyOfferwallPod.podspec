Pod::Spec.new do |spec|
  spec.name         = "VeseyOfferwallPod"
  spec.version      = "0.0.9"
  spec.summary      = "VeseyOfferwallPod"
  spec.homepage     = "https://bitbucket.org/veseystudios/offerwall-pod"
  spec.license      = { :type => "MIT", :file => "LICENSE" }
  spec.author       = { "Raul Salcedo" => "rsalcedo@fluentco.com" }
  spec.source       = { :git => "https://bitbucket.org/veseystudios/offerwall-pod.git", :tag => "#{spec.version}" }
  spec.platform     = :ios
  spec.ios.deployment_target = "10.0"
  spec.ios.vendored_frameworks = "VeseyOfferwall.framework"
  spec.pod_target_xcconfig = {
    'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64'
  }
  spec.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
end
